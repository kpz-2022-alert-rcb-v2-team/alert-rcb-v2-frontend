import { createApp } from 'vue'
import App from './App.vue'
import Amplify, {API, Auth} from "aws-amplify";
import awsExports from "./aws-exports.js";
import { Quasar } from 'quasar'

//Amplify.configure(awsExports);
Amplify.configure({
  Auth: {
      
      // REQUIRED - Amazon Cognito Region
      region: 'eu-central-1',

      // OPTIONAL - Amazon Cognito User Pool ID
      userPoolId: 'eu-central-1_UG3cvvrLi',

      // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
      userPoolWebClientId: '73br9gntbcdtbls3termru97a6',
  },
  API: {
    endpoints: [
        {
            name: "Endpoint",
            endpoint: "https://hy4y52k61e.execute-api.eu-central-1.amazonaws.com/prod/",
            // custom_header: async () => { 
            //   return { Authorization: `Bearer ${(await Auth.currentSession()).getIdToken().getJwtToken()}` }
            // }
        }
    ]
}
});

//const currentConfig = Auth.configure();

import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-icons/material-icons.css'
import '@quasar/extras/material-icons-outlined/material-icons-outlined.css'
import '@quasar/extras/material-icons-round/material-icons-round.css'
import '@quasar/extras/material-icons-sharp/material-icons-sharp.css'
import '@quasar/extras/mdi-v6/mdi-v6.css'
import '@quasar/extras/fontawesome-v5/fontawesome-v5.css'
import '@quasar/extras/fontawesome-v6/fontawesome-v6.css'
import '@quasar/extras/ionicons-v4/ionicons-v4.css'
import '@quasar/extras/eva-icons/eva-icons.css'
import '@quasar/extras/themify/themify.css'
import '@quasar/extras/line-awesome/line-awesome.css'
import '@quasar/extras/bootstrap-icons/bootstrap-icons.css'

// Import Quasar css
import 'quasar/src/css/index.sass'

// Assumes your root component is App.vue
// and placed in same folder as main.js

const myApp = createApp(App)

myApp.use(Quasar, {
  plugins: {}, // import Quasar plugins and add here
  /*
  config: {
    brand: {
      // primary: '#e46262',
      // ... or all other brand colors
    },
    notify: {...}, // default set of options for Notify Quasar plugin
    loading: {...}, // default set of options for Loading Quasar plugin
    loadingBar: { ... }, // settings for LoadingBar Quasar plugin
    // ..and many more (check Installation card on each Quasar component/directive/plugin)
  }
  */
})

// Assumes you have a <div id="app"></div> in your index.html
myApp.mount('#app')
